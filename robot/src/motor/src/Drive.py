#!/usr/bin/env python
import rospy
import time
from std_msgs.msg import String
from Motor import Motor
from test_server.msg import Pos
from Distance import Dist
from object_recognition.msg import PictureRequest
dis = Dist(0)
stop = True
def callback(data,args): # sonar ophalen
    rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)
    dis.dis = float(data.data)
    if float(data.data)>= 15:
    	args[0].driveAllowed = True
    else:
        args[0].driveAllowed = False

def testback(data,args): # test talker 1 of 2
    rospy.loginfo(rospy.get_caller_id() + "I heard %s",data.data)
    global stop
    if data.data == "2" or data.data == str(rospy.get_caller_id()): # 2 robot of specifiek id mag hij volgende positie opvragen
        publisher(args[0].cur_pos,args[1])
    elif data.data == "s":
        args[0].stopDriving()
    elif data.data == "end":
       stop == False
    elif data.data == "stop" and stop == True:
        msg = Pos()
        msg.pos = args[0].cur_pos
        msg.id = str(rospy.get_caller_id())
        args[2].publish(msg)


def resback(data,args): # get result, to drive or not to drive, that's the question
    if rospy.get_caller_id() == data.id:
        rospy.loginfo(rospy.get_caller_id() + "I heard %s",data.dir)
        if data.dir == args[0].look_dir:
            if args[0].driveAllowed:
                start = round(dis.dis,1)
                end = start -17
                if end <= 3:
                    end = 3.5
                count = 0
                args[0].driveForward()
                while round(dis.dis,1) >= round(start,1) - 20:
                    print("driving")
                    print(dis.dis)
                    if dis.dis < end:
                        count +=1
                        if count == 3:
                            break
                args[0].stopDriving()
                args[0].cur_pos = data.pos

                msg = Pos()
                msg.pos = data.pos
                msg.old_pos = data.old_pos
                msg.id = rospy.get_caller_id()
                args[2].publish(msg)
                print("pubbed_conf_forw")

            else:
                msg = PictureRequest( id ="/camera_2")
                msg.pos = data.pos
                msg.rob_id= rospy.get_caller_id()
                args[3].publish(msg)
                print("pubbed obj")


        else:
            if data.dir == "d":
    	        if args[0].look_dir == "r":
	            args[0].rotateRight(0.43)
                elif args[0].look_dir == "u":
                    args[0].rotateRight(0.43)
                    time.sleep(1)
                    args[0].rotateRight(0.43)
	        elif args[0].look_dir == "l":
                    args[0].rotateLeft(0.43)
                args[0].look_dir = "d"
            elif data.dir == "l":
                if args[0].look_dir == "r":
                    args[0].rotateLeft(0.43)
                    time.sleep(1)
                    args[0].rotateLeft(0.43)
                elif args[0].look_dir == "u":
                    args[0].rotateLeft(0.43)
                elif args[0].look_dir == "d":
                    args[0].rotateRight(0.43)
                args[0].look_dir = "l"

            elif data.dir == "u":
                if args[0].look_dir == "r":
                    args[0].rotateLeft(0.43)
                elif args[0].look_dir == "d":
                    args[0].rotateRight(0.43)
                    time.sleep(1)
                    args[0].rotateRight(0.43)
                elif args[0].look_dir == "l":
                    args[0].rotateRight(0.43)
                args[0].look_dir = "u"

            elif data.dir == "r":
                if args[0].look_dir == "d":
                    args[0].rotateLeft(0.43)
                elif args[0].look_dir == "u":
                    args[0].rotateRight(0.43)
                elif args[0].look_dir == "l":
                    args[0].rotateRight(0.43)
                    time.sleep(1)
                    args[0].rotateRight(0.43)
                args[0].look_dir = "r"
            time.sleep(1)

            if args[0].driveAllowed == False:
                msg = PictureRequest(id = "/camera_2")
                msg.pos = data.pos
                msg.rob_id= rospy.get_caller_id()
                args[3].publish(msg)
                print("pubbed obj")
            else:
                time.sleep(1)
                start = round(dis.dis,2)
                end = start -17
                if end <= 3:
                    end = 3.5
                count = 0
                args[0].driveForward()
                while dis.dis >= round(start,1) - 20:
                    print("driving")
                    print(dis.dis)
                    if dis.dis < end:
                        count += 1
                        if count == 3:
                            break

                args[0].stopDriving()

                args[0].cur_pos = data.pos
                msg = Pos()
                msg.pos = data.pos
                msg.old_pos = data.old_pos
                msg.id = rospy.get_caller_id()
                args[2].publish(msg)
                print("pubbed conf")


def listener():
    mot = Motor(20,16,21,24,23,25)
    mot.config()
    mot.stopDriving()
    rospy.init_node('drive_node', anonymous=True)
    pub = rospy.Publisher("position_data_node", Pos,queue_size=10)
    rospy.Subscriber("sonar_data_2", String, callback,(mot,'1'))
    pub_conf = rospy.Publisher("confirm_pos", Pos,queue_size=10)
    pub_obj = rospy.Publisher("picture_request",PictureRequest,queue_size=10)
    pub_drive = rospy.Publisher('command_data_node', String, queue_size=10)
    pub_short = rospy.Publisher('short_route', Pos,queue_size=10)
    rospy.Subscriber("command_data_node", String, testback,(mot,pub,pub_short))
    rospy.Subscriber("position_result_data_node", Pos, resback,(mot,pub,pub_conf,pub_obj,pub_drive))
    print("klaar")
    # spin() simply keeps python from exiting until this node is stopped
    mot.look_dir = "d"

    rospy.spin()
    mot.clean()

def publisher(cur_pos,pub):
    msg = Pos()
    msg.pos = cur_pos
    msg.id = str(rospy.get_caller_id())
    pub.publish(msg)
    print("pubbed")


if __name__ == '__main__':
    print("iets")
    listener()


