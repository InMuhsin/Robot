#!/usr/bin/env python

from Motor import Motor
import rospy

mot = Motor(20, 16, 21, 24, 23, 25)

mot.config()
mot.stopDriving()
mot.driveForward()
rospy.sleep(2)
mot.stopDriving()
