#!/usr/bin/env python
import RPi.GPIO as GPIO
from time import sleep

GPIO.setmode(GPIO.BCM)


class Motor():
    def __init__(self, input1, input2, enable1, input3, input4, enable2):
        self.in1 = input1
        self.in2 = input2
        self.en1 = enable1
        self.in3 = input3
        self.in4 = input4
        self.en2 = enable2
        self.driveAllowed = True
        self.ena1 = "1"
        self.ena2 = "2"
        self.cur_pos = [0, 0]
        self.look_dir = "d"

    def config(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.in1, GPIO.OUT)
        GPIO.setup(self.in2, GPIO.OUT)
        GPIO.setup(self.en1, GPIO.OUT)
        GPIO.setup(self.in3, GPIO.OUT)
        GPIO.setup(self.in4, GPIO.OUT)
        GPIO.setup(self.en2, GPIO.OUT)
        GPIO.output(self.in1, GPIO.LOW)
        GPIO.output(self.in2, GPIO.LOW)
        GPIO.output(self.in3, GPIO.LOW)
        GPIO.output(self.in4, GPIO.LOW)

        self.ena1 = GPIO.PWM(self.en1, 100)
        self.ena2 = GPIO.PWM(self.en2, 100)
        self.ena1.start(0)
        self.ena2.start(0)

    def driveForward(self):
        self.ena1.ChangeDutyCycle(62)
        self.ena2.ChangeDutyCycle(54)
        GPIO.output(self.in1, GPIO.HIGH)
        GPIO.output(self.in2, GPIO.LOW)
        GPIO.output(self.in3, GPIO.HIGH)
        GPIO.output(self.in4, GPIO.LOW)


    def driveBackward(self, driveTime):
        self.ena1.ChangeDutyCycle(75)
        self.ena2.ChangeDutyCycle(75)

        GPIO.output(self.in1, GPIO.LOW)
        GPIO.output(self.in2, GPIO.HIGH)
        GPIO.output(self.in3, GPIO.LOW)
        GPIO.output(self.in4, GPIO.HIGH)
        sleep(driveTime)
        self.stopDriving()

    def rotateLeft(self, driveTime):
        self.ena1.ChangeDutyCycle(60)
        self.ena2.ChangeDutyCycle(60)

        GPIO.output(self.in1, GPIO.HIGH)
        GPIO.output(self.in2, GPIO.LOW)
        GPIO.output(self.in3, GPIO.LOW)
        GPIO.output(self.in4, GPIO.HIGH)
        sleep(driveTime)
        self.stopDriving()

    def rotateRight(self, driveTime):
        self.ena1.ChangeDutyCycle(58)
        self.ena2.ChangeDutyCycle(60)

        GPIO.output(self.in1, GPIO.LOW)
        GPIO.output(self.in2, GPIO.HIGH)
        GPIO.output(self.in3, GPIO.HIGH)
        GPIO.output(self.in4, GPIO.LOW)
        sleep(driveTime)
        self.stopDriving()

    def stopDriving(self):
        GPIO.output(self.in1, GPIO.LOW)
        GPIO.output(self.in2, GPIO.LOW)
        GPIO.output(self.in3, GPIO.LOW)
        GPIO.output(self.in4, GPIO.LOW)

    def clean(self):
        GPIO.cleanup()
