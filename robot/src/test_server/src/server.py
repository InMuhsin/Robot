#!/usr/bin/env python
from Map import Map
import numpy as np
import rospy
from std_msgs.msg import String
import random
from test_server.msg import Pos

def callback(data,args):
    print("hallo")
    rospy.loginfo(rospy.get_caller_id() + "I heard %s",data.pos)
    cur_pos=list(data.pos)
    print(cur_pos)
    map = args[0]
    res = map.get_free_spaces(cur_pos)

    if 0 in res:
        i = random.randint(0,3)
        p = res[i]
        if p != 0:
            while p != 0:
                i = random.randint(0, 3)
                p = res[i]
    elif 1 in res:
        i = random.randint(0, 3)
        p = res[i]
        if p != 1:
            while p != 1:
                i = random.randint(0, 3)
                p = res[i]

    old_pos = cur_pos
    if i == 0:
        cur_pos = [cur_pos[0],cur_pos[1]+1]
    elif i == 1:
        cur_pos = [cur_pos[0]+1,cur_pos[1]]
    elif i == 2:
            cur_pos = [cur_pos[0],cur_pos[1]-1]
    elif i == 3:
        cur_pos = [cur_pos[0]-1,cur_pos[1]]
    map.set_pos(cur_pos[0],cur_pos[1],9)
    map.set_pos(old_pos[0], old_pos[1], 1)
    pub = args[1]
    msg = Pos()
    msg.pos = cur_pos
    pub.publish(msg)
    print("pubbed")

    print(map.map)


def listener():

    map = Map((10,10))
    map.set_pos(0,0,1)
    map.set_pos(3,4,3)
    map.set_pos(0,7,3)
    map.set_pos(8,5,3)
    map.set_pos(9,2,3)
    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    print(map.map)
    rospy.init_node('server_node', anonymous=True)
    pub = rospy.Publisher("position_result_data_node", Pos,queue_size=10)
    pub = rospy.Publisher("shortest_route_res_node", Pos,queue_size=10)
    rospy.Subscriber("position_data_node", Pos, callback,(map,pub))
    rospy.Subscriber("shortest_route_node", Pos, callback,(map,pub_route))
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':

    listener()

