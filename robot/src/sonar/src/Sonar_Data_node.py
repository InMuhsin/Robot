#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from Sonar import Sensor
import sys

def talker():
    pub = rospy.Publisher('sonar_data_2', String, queue_size=10)
    rospy.init_node('sonar_node_2', anonymous=False)
    rate = rospy.Rate(10) # 10hz
    sen = Sensor(27,17)

    while True:
        data =  str(sen.distance())
        pub.publish(data)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except Exception as ex:
        print(ex)
        print("error")
