#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import String


def talker():
    pub = rospy.Publisher('command_data_node', String, queue_size=10)
    rospy.init_node('server_node', anonymous=True)

    data = "a"

    while data != "q":
        data = raw_input("d for drive s for stop: ")
        print(data)
        rospy.loginfo(str(data))
        pub.publish(str(data))


if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass

