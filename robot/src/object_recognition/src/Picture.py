#!/usr/bin/env python
import rospy
from object_recognition.msg import PictureRequest, Picture

from rospy.numpy_msg import numpy_msg
# from std_msgs.msg import UInt8

import time
import picamera
import picamera.array

import numpy as np


def callback(data, args):
    print(data.id)
    if data.id == rospy.get_caller_id():
        rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.id)

        with picamera.PiCamera() as camera:
            with picamera.array.PiRGBArray(camera) as output:
                shape = (224, 224, 3)
                camera.resolution = (shape[:-1])
                print(shape[:-1])
                camera.start_preview()
                time.sleep(0.5)
                camera.capture(output, 'rgb')
                # print(output.array.shape)
                # print(output.array)
                pic = np.asarray(output.array).flatten().tolist()
                msg = Picture(id=rospy.get_caller_id(), picture=pic, shape=shape, pos=data.pos, rob_id=data.rob_id)
                # print(output.array.dtype)

        print(rospy.get_caller_id)
        rospy.loginfo(msg)
        pub = args[0]
        pub.publish(msg)


def listener():
    rospy.init_node('camera_2')
    pub = rospy.Publisher("picture", Picture, queue_size=10)
    rospy.Subscriber("picture_request", PictureRequest, callback, (pub,))
    # camera request

    print(rospy.get_caller_id())

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()


if __name__ == '__main__':
    listener()
