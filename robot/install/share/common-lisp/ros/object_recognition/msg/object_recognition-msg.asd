
(cl:in-package :asdf)

(defsystem "object_recognition-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "Picture" :depends-on ("_package_Picture"))
    (:file "_package_Picture" :depends-on ("_package"))
    (:file "PictureRequest" :depends-on ("_package_PictureRequest"))
    (:file "_package_PictureRequest" :depends-on ("_package"))
  ))