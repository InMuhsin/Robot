;; Auto-generated. Do not edit!


(when (boundp 'object_recognition::Picture)
  (if (not (find-package "OBJECT_RECOGNITION"))
    (make-package "OBJECT_RECOGNITION"))
  (shadow 'Picture (find-package "OBJECT_RECOGNITION")))
(unless (find-package "OBJECT_RECOGNITION::PICTURE")
  (make-package "OBJECT_RECOGNITION::PICTURE"))

(in-package "ROS")
;;//! \htmlinclude Picture.msg.html


(defclass object_recognition::Picture
  :super ros::object
  :slots (_id _picture _shape _pos _rob_id ))

(defmethod object_recognition::Picture
  (:init
   (&key
    ((:id __id) "")
    ((:picture __picture) (make-array 0 :initial-element 0 :element-type :integer))
    ((:shape __shape) (make-array 3 :initial-element 0 :element-type :integer))
    ((:pos __pos) (make-array 0 :initial-element 0 :element-type :integer))
    ((:rob_id __rob_id) "")
    )
   (send-super :init)
   (setq _id (string __id))
   (setq _picture __picture)
   (setq _shape __shape)
   (setq _pos __pos)
   (setq _rob_id (string __rob_id))
   self)
  (:id
   (&optional __id)
   (if __id (setq _id __id)) _id)
  (:picture
   (&optional __picture)
   (if __picture (setq _picture __picture)) _picture)
  (:shape
   (&optional __shape)
   (if __shape (setq _shape __shape)) _shape)
  (:pos
   (&optional __pos)
   (if __pos (setq _pos __pos)) _pos)
  (:rob_id
   (&optional __rob_id)
   (if __rob_id (setq _rob_id __rob_id)) _rob_id)
  (:serialization-length
   ()
   (+
    ;; string _id
    4 (length _id)
    ;; int32[] _picture
    (* 4    (length _picture)) 4
    ;; int32[3] _shape
    (* 4    3)
    ;; int32[] _pos
    (* 4    (length _pos)) 4
    ;; string _rob_id
    4 (length _rob_id)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _id
       (write-long (length _id) s) (princ _id s)
     ;; int32[] _picture
     (write-long (length _picture) s)
     (dotimes (i (length _picture))
       (write-long (elt _picture i) s)
       )
     ;; int32[3] _shape
     (dotimes (i 3)
       (write-long (elt _shape i) s)
       )
     ;; int32[] _pos
     (write-long (length _pos) s)
     (dotimes (i (length _pos))
       (write-long (elt _pos i) s)
       )
     ;; string _rob_id
       (write-long (length _rob_id) s) (princ _rob_id s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _id
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _id (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; int32[] _picture
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _picture (instantiate integer-vector n))
     (dotimes (i n)
     (setf (elt _picture i) (sys::peek buf ptr- :integer)) (incf ptr- 4)
     ))
   ;; int32[3] _shape
   (dotimes (i (length _shape))
     (setf (elt _shape i) (sys::peek buf ptr- :integer)) (incf ptr- 4)
     )
   ;; int32[] _pos
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _pos (instantiate integer-vector n))
     (dotimes (i n)
     (setf (elt _pos i) (sys::peek buf ptr- :integer)) (incf ptr- 4)
     ))
   ;; string _rob_id
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _rob_id (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(setf (get object_recognition::Picture :md5sum-) "dfd344394c9c4e6d19934fbcb8faafbe")
(setf (get object_recognition::Picture :datatype-) "object_recognition/Picture")
(setf (get object_recognition::Picture :definition-)
      "string id
int32[] picture
int32[3] shape
int32[] pos
string rob_id

")



(provide :object_recognition/Picture "dfd344394c9c4e6d19934fbcb8faafbe")


