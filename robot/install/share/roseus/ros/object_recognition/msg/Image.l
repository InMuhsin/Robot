;; Auto-generated. Do not edit!


(when (boundp 'object_recognition::Image)
  (if (not (find-package "OBJECT_RECOGNITION"))
    (make-package "OBJECT_RECOGNITION"))
  (shadow 'Image (find-package "OBJECT_RECOGNITION")))
(unless (find-package "OBJECT_RECOGNITION::IMAGE")
  (make-package "OBJECT_RECOGNITION::IMAGE"))

(in-package "ROS")
;;//! \htmlinclude Image.msg.html


(defclass object_recognition::Image
  :super ros::object
  :slots (_pixel ))

(defmethod object_recognition::Image
  (:init
   (&key
    ((:pixel __pixel) (make-array 0 :initial-element 0 :element-type :char))
    )
   (send-super :init)
   (setq _pixel __pixel)
   self)
  (:pixel
   (&optional __pixel)
   (if __pixel (setq _pixel __pixel)) _pixel)
  (:serialization-length
   ()
   (+
    ;; uint8[] _pixel
    (* 1    (length _pixel)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; uint8[] _pixel
     (write-long (length _pixel) s)
     (princ _pixel s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; uint8[] _pixel
   (let ((n (sys::peek buf ptr- :integer))) (incf ptr- 4)
     (setq _pixel (make-array n :element-type :char))
     (replace _pixel buf :start2 ptr-) (incf ptr- n))
   ;;
   self)
  )

(setf (get object_recognition::Image :md5sum-) "a60ee3c027b6739afa88682b07eb9bfc")
(setf (get object_recognition::Image :datatype-) "object_recognition/Image")
(setf (get object_recognition::Image :definition-)
      "uint8[] pixel

")



(provide :object_recognition/Image "a60ee3c027b6739afa88682b07eb9bfc")


