;; Auto-generated. Do not edit!


(when (boundp 'test_server::Pos)
  (if (not (find-package "TEST_SERVER"))
    (make-package "TEST_SERVER"))
  (shadow 'Pos (find-package "TEST_SERVER")))
(unless (find-package "TEST_SERVER::POS")
  (make-package "TEST_SERVER::POS"))

(in-package "ROS")
;;//! \htmlinclude Pos.msg.html


(defclass test_server::Pos
  :super ros::object
  :slots (_pos _dir _old_pos _id ))

(defmethod test_server::Pos
  (:init
   (&key
    ((:pos __pos) (make-array 0 :initial-element 0 :element-type :integer))
    ((:dir __dir) "")
    ((:old_pos __old_pos) (make-array 0 :initial-element 0 :element-type :integer))
    ((:id __id) "")
    )
   (send-super :init)
   (setq _pos __pos)
   (setq _dir (string __dir))
   (setq _old_pos __old_pos)
   (setq _id (string __id))
   self)
  (:pos
   (&optional __pos)
   (if __pos (setq _pos __pos)) _pos)
  (:dir
   (&optional __dir)
   (if __dir (setq _dir __dir)) _dir)
  (:old_pos
   (&optional __old_pos)
   (if __old_pos (setq _old_pos __old_pos)) _old_pos)
  (:id
   (&optional __id)
   (if __id (setq _id __id)) _id)
  (:serialization-length
   ()
   (+
    ;; int32[] _pos
    (* 4    (length _pos)) 4
    ;; string _dir
    4 (length _dir)
    ;; int32[] _old_pos
    (* 4    (length _old_pos)) 4
    ;; string _id
    4 (length _id)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int32[] _pos
     (write-long (length _pos) s)
     (dotimes (i (length _pos))
       (write-long (elt _pos i) s)
       )
     ;; string _dir
       (write-long (length _dir) s) (princ _dir s)
     ;; int32[] _old_pos
     (write-long (length _old_pos) s)
     (dotimes (i (length _old_pos))
       (write-long (elt _old_pos i) s)
       )
     ;; string _id
       (write-long (length _id) s) (princ _id s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int32[] _pos
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _pos (instantiate integer-vector n))
     (dotimes (i n)
     (setf (elt _pos i) (sys::peek buf ptr- :integer)) (incf ptr- 4)
     ))
   ;; string _dir
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _dir (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; int32[] _old_pos
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _old_pos (instantiate integer-vector n))
     (dotimes (i n)
     (setf (elt _old_pos i) (sys::peek buf ptr- :integer)) (incf ptr- 4)
     ))
   ;; string _id
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _id (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(setf (get test_server::Pos :md5sum-) "a1d3a5801afdb2217cf89792d3b5eadb")
(setf (get test_server::Pos :datatype-) "test_server/Pos")
(setf (get test_server::Pos :definition-)
      "int32[] pos
string  dir
int32[] old_pos
string  id

")



(provide :test_server/Pos "a1d3a5801afdb2217cf89792d3b5eadb")


