// Auto-generated. Do not edit!

// (in-package object_recognition.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class Image {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.pixel = null;
    }
    else {
      if (initObj.hasOwnProperty('pixel')) {
        this.pixel = initObj.pixel
      }
      else {
        this.pixel = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Image
    // Serialize message field [pixel]
    bufferOffset = _arraySerializer.uint8(obj.pixel, buffer, bufferOffset, null);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Image
    let len;
    let data = new Image(null);
    // Deserialize message field [pixel]
    data.pixel = _arrayDeserializer.uint8(buffer, bufferOffset, null)
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.pixel.length;
    return length + 4;
  }

  static datatype() {
    // Returns string type for a message object
    return 'object_recognition/Image';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'a60ee3c027b6739afa88682b07eb9bfc';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    uint8[] pixel
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Image(null);
    if (msg.pixel !== undefined) {
      resolved.pixel = msg.pixel;
    }
    else {
      resolved.pixel = []
    }

    return resolved;
    }
};

module.exports = Image;
