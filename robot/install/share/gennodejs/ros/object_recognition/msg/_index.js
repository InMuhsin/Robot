
"use strict";

let PictureRequest = require('./PictureRequest.js');
let Picture = require('./Picture.js');

module.exports = {
  PictureRequest: PictureRequest,
  Picture: Picture,
};
