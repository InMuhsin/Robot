// Auto-generated. Do not edit!

// (in-package object_recognition.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class Picture {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.id = null;
      this.picture = null;
      this.shape = null;
      this.pos = null;
      this.rob_id = null;
    }
    else {
      if (initObj.hasOwnProperty('id')) {
        this.id = initObj.id
      }
      else {
        this.id = '';
      }
      if (initObj.hasOwnProperty('picture')) {
        this.picture = initObj.picture
      }
      else {
        this.picture = [];
      }
      if (initObj.hasOwnProperty('shape')) {
        this.shape = initObj.shape
      }
      else {
        this.shape = new Array(3).fill(0);
      }
      if (initObj.hasOwnProperty('pos')) {
        this.pos = initObj.pos
      }
      else {
        this.pos = [];
      }
      if (initObj.hasOwnProperty('rob_id')) {
        this.rob_id = initObj.rob_id
      }
      else {
        this.rob_id = '';
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Picture
    // Serialize message field [id]
    bufferOffset = _serializer.string(obj.id, buffer, bufferOffset);
    // Serialize message field [picture]
    bufferOffset = _arraySerializer.int32(obj.picture, buffer, bufferOffset, null);
    // Check that the constant length array field [shape] has the right length
    if (obj.shape.length !== 3) {
      throw new Error('Unable to serialize array field shape - length must be 3')
    }
    // Serialize message field [shape]
    bufferOffset = _arraySerializer.int32(obj.shape, buffer, bufferOffset, 3);
    // Serialize message field [pos]
    bufferOffset = _arraySerializer.int32(obj.pos, buffer, bufferOffset, null);
    // Serialize message field [rob_id]
    bufferOffset = _serializer.string(obj.rob_id, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Picture
    let len;
    let data = new Picture(null);
    // Deserialize message field [id]
    data.id = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [picture]
    data.picture = _arrayDeserializer.int32(buffer, bufferOffset, null)
    // Deserialize message field [shape]
    data.shape = _arrayDeserializer.int32(buffer, bufferOffset, 3)
    // Deserialize message field [pos]
    data.pos = _arrayDeserializer.int32(buffer, bufferOffset, null)
    // Deserialize message field [rob_id]
    data.rob_id = _deserializer.string(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.id.length;
    length += 4 * object.picture.length;
    length += 4 * object.pos.length;
    length += object.rob_id.length;
    return length + 28;
  }

  static datatype() {
    // Returns string type for a message object
    return 'object_recognition/Picture';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'dfd344394c9c4e6d19934fbcb8faafbe';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    string id
    int32[] picture
    int32[3] shape
    int32[] pos
    string rob_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Picture(null);
    if (msg.id !== undefined) {
      resolved.id = msg.id;
    }
    else {
      resolved.id = ''
    }

    if (msg.picture !== undefined) {
      resolved.picture = msg.picture;
    }
    else {
      resolved.picture = []
    }

    if (msg.shape !== undefined) {
      resolved.shape = msg.shape;
    }
    else {
      resolved.shape = new Array(3).fill(0)
    }

    if (msg.pos !== undefined) {
      resolved.pos = msg.pos;
    }
    else {
      resolved.pos = []
    }

    if (msg.rob_id !== undefined) {
      resolved.rob_id = msg.rob_id;
    }
    else {
      resolved.rob_id = ''
    }

    return resolved;
    }
};

module.exports = Picture;
