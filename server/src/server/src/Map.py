#!/usr/bin/env python
import numpy as np

class Map():
    def __init__(self,shape):
        self.map = np.zeros(shape=(5,5))

    def set_pos(self,row,column,value):
        self.map[row][column]=value

    def get_value_pos(self,row,column):
        val = self.map[row][column]
        return val

    def get_free_spaces(self,current_pos):
        if (current_pos[1] +1  < 5):
            val_r = self.get_value_pos(current_pos[0], current_pos[1] + 1)
        else:
            val_r = 3

        if (current_pos[0] + 1 < 5):
             val_d = self.get_value_pos(current_pos[0]+1, current_pos[1] )
        else:
             val_d = 3

        if(current_pos[1] - 1 > -1):
             val_l = self.get_value_pos(current_pos[0], current_pos[1] - 1)
        else:
             val_l = 3

        if (current_pos[0] - 1 > -1):
             val_u = self.get_value_pos(current_pos[0]-1, current_pos[1])
        else:
             val_u = 3

        return [val_r,val_d,val_l,val_u]

