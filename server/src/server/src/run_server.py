#!/usr/bin/env python
from Map import Map
import numpy as np
import rospy
from std_msgs.msg import String
import random
from server.msg import Pos, Obj, Route
from collections import defaultdict
import time
id_dict = {}
all_stop = False
end_route = []

def dijsktra(graph, initial, end):
    # shortest paths is a dict of nodes
    # whose value is a tuple of (previous node, weight)
    shortest_paths = {initial: (None, 0)}
    current_node = initial
    visited = set()

    while current_node != end:
        visited.add(current_node)
        destinations = graph.edges[current_node]
        weight_to_current_node = shortest_paths[current_node][1]

        for next_node in destinations:
            weight = graph.weights[(current_node, next_node)] + weight_to_current_node
            if next_node not in shortest_paths:
                shortest_paths[next_node] = (current_node, weight)
            else:
                current_shortest_weight = shortest_paths[next_node][1]
                if current_shortest_weight > weight:
                    shortest_paths[next_node] = (current_node, weight)

        next_destinations = {node: shortest_paths[node] for node in shortest_paths if node not in visited}
        if not next_destinations:
            return "Route Not Possible"
        # next node is the destination with the lowest weight
        current_node = min(next_destinations, key=lambda k: next_destinations[k][1])

    # Work back through destinations in shortest path
    path = []
    while current_node is not None:
        path.append(current_node)
        next_node = shortest_paths[current_node][0]
        current_node = next_node
    # Reverse path252522322333202000220022000
    path = path[::-1]
    return path

class Graph():
    def __init__(self):
        """
        self.edges is a dict of all possible next nodes
        e.g. {'X': ['A', 'B', 'C', 'E'], ...}
        self.weights has all the weights between two nodes,
        with the two nodes as a tuple as the key
        e.g. {('X', 'A'): 7, ('X', 'B'): 2, ...}
        """
        self.edges = defaultdict(list)
        self.weights = {}

    def add_edge(self, from_node, to_node, weight):
        # Note: assumes edges are bi-directional
        self.edges[from_node].append(to_node)
        self.edges[to_node].append(from_node)
        self.weights[(from_node, to_node)] = weight
        self.weights[(to_node, from_node)] = weight


def callback_short(data,args): # dijkstra shortest path
    graph = Graph()
    print("dijk")
    reflect_map = np.array([
        [0, 1, 2, 3, 4],
        [5, 6, 7, 8, 9],
        [10, 11, 12, 13, 14],
        [15, 16, 17, 18, 19],
        [20, 21, 22, 23, 24]])

    map = args[1]

    edges = []
    row_counter = 0
    column_counter = 0

    for i in range(0, 24):
        if (i != 4 and i != 9 and i != 14 and i != 19 and i != 24):

            val = map.map[row_counter][column_counter + 1]
            if val == 0:
                val = 5000
            elif val == 1:
                val = 1
            elif val > 9 and val <40:
                val = 100
            elif val >45:
                val = 5000
            edges.append((str(i), str(i + 1), val))

        if (i < 20):
            val = map.map[row_counter + 1][column_counter]
            if val == 0:
                val = 5000
            elif val > 0 and val < 9:
                val = 1
            elif val > 9 and val < 40:
                val = 100
            elif val > 45:
                val = 5000

            edges.append((str(i), str(i + 5), val))
        column_counter += 1
        if (i == 4 or i == 9 or i == 14 or i == 19 or i == 24):
            row_counter += 1
            column_counter = 0
    for edge in edges:
        graph.add_edge(*edge)
    global end_route
    start = reflect_map[data.pos[0]][data.pos[1]]
    end = reflect_map[end_route[0]][end_route[1]]
    short = dijsktra(graph, str(start), str(end))
    print(short)
    res = []
    for number in short:
        itemindex = np.where(np.array(reflect_map) == int(number))
        res.append([itemindex[0][0],itemindex[1][0]])
    print(res)
    cur_pos = data.pos
    if len(res) > 2:
        new = res[1]
        dir = "none"
        print(res[1])
        old_pos = cur_pos
        if new[1] == old_pos[1] +1 :
            cur_pos = new
            dir = "r"
        elif new[0] == old_pos[0] +1 :
            cur_pos = new
            dir = "d"
        elif new[1] ==old_pos[1]-1:
            cur_pos = new
            dir = "l"
        elif new[0] == old_pos[0]-1:
            cur_pos = new
            dir = "u"

        msg = Pos()
        msg.pos = cur_pos
        msg.old_pos = old_pos
        msg.dir = dir
        msg.id = data.id
        print(msg.pos)
        print(msg.dir)
        args[0].publish(msg)
        print("pubbed")


###########





def callback(data,args): #bepaal eerst volgende plaats
    rospy.loginfo(rospy.get_caller_id() + "I heard %s",data.pos)
    print("next pos")
    cur_pos=list(data.pos)
    map = args[0]
    res = map.get_free_spaces(cur_pos)
    i = -1
    if 0 in res:
        i = random.randint(0,3)
        p = res[i]
        if p != 0:
            while p != 0:
                i = random.randint(0, 3)
                p = res[i]

    elif 1 in res:
        i = random.randint(0, 3)
        p = res[i]
        if p != 1:
            while p != 1:
                i = random.randint(0, 3)
                p = res[i]
    dir = "none"
    old_pos = cur_pos
    if i == 0:
        cur_pos = [cur_pos[0],cur_pos[1]+1]
	dir = "r"
    elif i == 1:
        cur_pos = [cur_pos[0]+1,cur_pos[1]]
	dir = "d"
    elif i == 2:
        cur_pos = [cur_pos[0],cur_pos[1]-1]
	dir = "l"
    elif i == 3:
        cur_pos = [cur_pos[0]-1,cur_pos[1]]
	dir = "u"

    pub = args[1]
    msg = Pos()
    msg.pos = cur_pos
    msg.old_pos = old_pos
    msg.dir = dir
    msg.id = data.id
    pub.publish(msg)
    print("pubbed_1")


def callback2(data,args): # object detecteerd
    rospy.loginfo(rospy.get_caller_id() + "I heard %s",data.pos)
    res = data.res
    print(res)
    global all_stop
    global end_route
    if res == "True" and all_stop == False:
        args[0].set_pos(data.pos[0],data.pos[1],100)
        all_stop = True
        end_route = data.pos
        args[1].publish("end")

    else:
        if all_stop != True:
            args[0].set_pos(data.pos[0],data.pos[1],50)
            args[1].publish(data.rob_id)
        else:
            args[1].publish("stop")
    print(args[0].map)

def callback3(data,args): # confirm de positie (zeg daj op de plaats staat)
    rospy.loginfo(rospy.get_caller_id() + "I heard %s",data.pos)
    if data.id in id_dict.keys():
        args[0].set_pos(data.pos[0],data.pos[1],id_dict[data.id])
        args[0].set_pos(data.old_pos[0], data.old_pos[1], 1)
    else:
        if len(id_dict) ==0:
            id_dict[data.id] = 10
        else:
            print(type(id_dict.values()))
            temp_list = id_dict.values()
            temp_list.sort()
            val = temp_list[0]
            id_dict[data.id] = val + 1
    print(args[0].map)
    time.sleep(5)
    global all_stop
    if all_stop != True:
        args[1].publish(data.id)
    else:
        args[1].publish("stop")

def listener():

    map = Map((5,5))
    map.set_pos(0,0,1)
    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    print(map.map)
    rospy.init_node('server_node', anonymous=True)
    pub = rospy.Publisher("position_result_data_node", Pos,queue_size=10)
    pub_route = rospy.Publisher("shortest_route_res_node", Pos,queue_size=10)
    pub_drive = rospy.Publisher('command_data_node', String, queue_size=10)
    rospy.Subscriber("position_data_node", Pos, callback,(map,pub))
    rospy.Subscriber("object_result",Obj,callback2,(map,pub_drive))
    pub_drive = rospy.Publisher('command_data_node', String, queue_size=10)

    rospy.Subscriber("confirm_pos",Pos,callback3,(map,pub_drive))
    rospy.Subscriber("short_route",Pos,callback_short,(pub,map))
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':

    listener()


