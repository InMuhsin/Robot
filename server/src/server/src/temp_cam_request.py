#!/usr/bin/env python

import rospy
from server.msg import PictureRequest, Picture


def talker():
    rospy.init_node('request_obj')
    pub = rospy.Publisher('picture_request', PictureRequest, queue_size=10)
    # rate = rospy.Rate(0.1)  # 10s
    # while not rospy.is_shutdown():
    msg = PictureRequest(id="/camera_2")
    input_ = raw_input("> ")
    while input_ != "stop":
        rospy.loginfo(msg)
        pub.publish(msg)
        input_ = raw_input("> ")
        # rate.sleep()


if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
