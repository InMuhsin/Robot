#!/usr/bin/env python

import rospy
from server.msg import PictureRequest, Picture, Obj
from PictureModel import Model
import cv2
import numpy as np
import time
import traceback

i = 0


def callback(data, args):
    try:
        time_ = time.time()
        # model = args[1]
        model = Model()
        print time.time() - time_
        img = model.preprocess_img(data.picture, data.shape)
        img = model.rotate(img, 180)

        # cv_img = np.asarray(data.picture).reshape(data.shape)
        # print(cv_img.shape)
        # r, g, b = cv_img[:, :, 0].copy(), cv_img[:, :, 1].copy(), cv_img[:, :, 2].copy()
        # cv_img = np.array([b, g, r]).transpose()

        # print(cv_img[0, 0])
        # cv_img[:, [0, 1, 2]] = cv_img[:, [2, 1, 0]]
        # print(cv_img[-1, 0])
        # print(cv_img[0, -1])
        #
        # cv_img = model.rotate(cv_img, 180)
        #
        # global i
        # cv2.imwrite("/home/robot/Apps/Robot/img " + str(i) + ".jpg", cv_img)
        # i += 1
        res = model.is_valid(img)
        print(res)
        del model
        msg = Obj(res=str(res), pos=data.pos, rob_id=data.rob_id)
        args[0].publish(msg)
    except:
        msg = Obj(res=str(False), pos=data.pos, rob_id=data.rob_id)
        args[0].publish(msg)
        traceback.print_exc()


def listener():
    # model = Model()
    rospy.init_node('object_recognition')
    pub_res = rospy.Publisher("object_result", Obj, queue_size=10)
    rospy.Subscriber("picture", Picture, callback, (pub_res,))

    print(rospy.get_caller_id())

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()


if __name__ == '__main__':
    try:
        listener()
    except rospy.ROSInterruptException:
        pass
