// Generated by gencpp from file server/Obj.msg
// DO NOT EDIT!


#ifndef SERVER_MESSAGE_OBJ_H
#define SERVER_MESSAGE_OBJ_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace server
{
template <class ContainerAllocator>
struct Obj_
{
  typedef Obj_<ContainerAllocator> Type;

  Obj_()
    : res()
    , pos()
    , rob_id()  {
    }
  Obj_(const ContainerAllocator& _alloc)
    : res(_alloc)
    , pos(_alloc)
    , rob_id(_alloc)  {
  (void)_alloc;
    }



   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _res_type;
  _res_type res;

   typedef std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  _pos_type;
  _pos_type pos;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _rob_id_type;
  _rob_id_type rob_id;





  typedef boost::shared_ptr< ::server::Obj_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::server::Obj_<ContainerAllocator> const> ConstPtr;

}; // struct Obj_

typedef ::server::Obj_<std::allocator<void> > Obj;

typedef boost::shared_ptr< ::server::Obj > ObjPtr;
typedef boost::shared_ptr< ::server::Obj const> ObjConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::server::Obj_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::server::Obj_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace server

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': False}
// {'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg'], 'server': ['/home/robot/Apps/Robot/server/src/server/msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::server::Obj_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::server::Obj_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::server::Obj_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::server::Obj_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::server::Obj_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::server::Obj_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::server::Obj_<ContainerAllocator> >
{
  static const char* value()
  {
    return "bf448c37f9b2e18b1fceb325f2f29c80";
  }

  static const char* value(const ::server::Obj_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0xbf448c37f9b2e18bULL;
  static const uint64_t static_value2 = 0x1fceb325f2f29c80ULL;
};

template<class ContainerAllocator>
struct DataType< ::server::Obj_<ContainerAllocator> >
{
  static const char* value()
  {
    return "server/Obj";
  }

  static const char* value(const ::server::Obj_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::server::Obj_<ContainerAllocator> >
{
  static const char* value()
  {
    return "string res\n\
int32[] pos\n\
string rob_id\n\
";
  }

  static const char* value(const ::server::Obj_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::server::Obj_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.res);
      stream.next(m.pos);
      stream.next(m.rob_id);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct Obj_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::server::Obj_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::server::Obj_<ContainerAllocator>& v)
  {
    s << indent << "res: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.res);
    s << indent << "pos[]" << std::endl;
    for (size_t i = 0; i < v.pos.size(); ++i)
    {
      s << indent << "  pos[" << i << "]: ";
      Printer<int32_t>::stream(s, indent + "  ", v.pos[i]);
    }
    s << indent << "rob_id: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.rob_id);
  }
};

} // namespace message_operations
} // namespace ros

#endif // SERVER_MESSAGE_OBJ_H
