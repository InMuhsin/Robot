
(cl:in-package :asdf)

(defsystem "server-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "Obj" :depends-on ("_package_Obj"))
    (:file "_package_Obj" :depends-on ("_package"))
    (:file "Picture" :depends-on ("_package_Picture"))
    (:file "_package_Picture" :depends-on ("_package"))
    (:file "PictureRequest" :depends-on ("_package_PictureRequest"))
    (:file "_package_PictureRequest" :depends-on ("_package"))
    (:file "Pos" :depends-on ("_package_Pos"))
    (:file "_package_Pos" :depends-on ("_package"))
  ))