(cl:in-package server-msg)
(cl:export '(ID-VAL
          ID
          PICTURE-VAL
          PICTURE
          SHAPE-VAL
          SHAPE
          POS-VAL
          POS
          ROB_ID-VAL
          ROB_ID
))