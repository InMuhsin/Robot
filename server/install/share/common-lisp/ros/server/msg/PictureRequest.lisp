; Auto-generated. Do not edit!


(cl:in-package server-msg)


;//! \htmlinclude PictureRequest.msg.html

(cl:defclass <PictureRequest> (roslisp-msg-protocol:ros-message)
  ((id
    :reader id
    :initarg :id
    :type cl:string
    :initform "")
   (pos
    :reader pos
    :initarg :pos
    :type (cl:vector cl:integer)
   :initform (cl:make-array 0 :element-type 'cl:integer :initial-element 0))
   (rob_id
    :reader rob_id
    :initarg :rob_id
    :type cl:string
    :initform ""))
)

(cl:defclass PictureRequest (<PictureRequest>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PictureRequest>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PictureRequest)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name server-msg:<PictureRequest> is deprecated: use server-msg:PictureRequest instead.")))

(cl:ensure-generic-function 'id-val :lambda-list '(m))
(cl:defmethod id-val ((m <PictureRequest>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader server-msg:id-val is deprecated.  Use server-msg:id instead.")
  (id m))

(cl:ensure-generic-function 'pos-val :lambda-list '(m))
(cl:defmethod pos-val ((m <PictureRequest>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader server-msg:pos-val is deprecated.  Use server-msg:pos instead.")
  (pos m))

(cl:ensure-generic-function 'rob_id-val :lambda-list '(m))
(cl:defmethod rob_id-val ((m <PictureRequest>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader server-msg:rob_id-val is deprecated.  Use server-msg:rob_id instead.")
  (rob_id m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PictureRequest>) ostream)
  "Serializes a message object of type '<PictureRequest>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'id))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'id))
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'pos))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (cl:let* ((signed ele) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    ))
   (cl:slot-value msg 'pos))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'rob_id))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'rob_id))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PictureRequest>) istream)
  "Deserializes a message object of type '<PictureRequest>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'id) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'id) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'pos) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'pos)))
    (cl:dotimes (i __ros_arr_len)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:aref vals i) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296)))))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'rob_id) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'rob_id) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PictureRequest>)))
  "Returns string type for a message object of type '<PictureRequest>"
  "server/PictureRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PictureRequest)))
  "Returns string type for a message object of type 'PictureRequest"
  "server/PictureRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PictureRequest>)))
  "Returns md5sum for a message object of type '<PictureRequest>"
  "58437850ababa37644cd94b15d68462c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PictureRequest)))
  "Returns md5sum for a message object of type 'PictureRequest"
  "58437850ababa37644cd94b15d68462c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PictureRequest>)))
  "Returns full string definition for message of type '<PictureRequest>"
  (cl:format cl:nil "string id~%int32[] pos~%string rob_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PictureRequest)))
  "Returns full string definition for message of type 'PictureRequest"
  (cl:format cl:nil "string id~%int32[] pos~%string rob_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PictureRequest>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'id))
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'pos) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ 4)))
     4 (cl:length (cl:slot-value msg 'rob_id))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PictureRequest>))
  "Converts a ROS message object to a list"
  (cl:list 'PictureRequest
    (cl:cons ':id (id msg))
    (cl:cons ':pos (pos msg))
    (cl:cons ':rob_id (rob_id msg))
))
