;; Auto-generated. Do not edit!


(when (boundp 'server::Obj)
  (if (not (find-package "SERVER"))
    (make-package "SERVER"))
  (shadow 'Obj (find-package "SERVER")))
(unless (find-package "SERVER::OBJ")
  (make-package "SERVER::OBJ"))

(in-package "ROS")
;;//! \htmlinclude Obj.msg.html


(defclass server::Obj
  :super ros::object
  :slots (_res _pos _rob_id ))

(defmethod server::Obj
  (:init
   (&key
    ((:res __res) "")
    ((:pos __pos) (make-array 0 :initial-element 0 :element-type :integer))
    ((:rob_id __rob_id) "")
    )
   (send-super :init)
   (setq _res (string __res))
   (setq _pos __pos)
   (setq _rob_id (string __rob_id))
   self)
  (:res
   (&optional __res)
   (if __res (setq _res __res)) _res)
  (:pos
   (&optional __pos)
   (if __pos (setq _pos __pos)) _pos)
  (:rob_id
   (&optional __rob_id)
   (if __rob_id (setq _rob_id __rob_id)) _rob_id)
  (:serialization-length
   ()
   (+
    ;; string _res
    4 (length _res)
    ;; int32[] _pos
    (* 4    (length _pos)) 4
    ;; string _rob_id
    4 (length _rob_id)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _res
       (write-long (length _res) s) (princ _res s)
     ;; int32[] _pos
     (write-long (length _pos) s)
     (dotimes (i (length _pos))
       (write-long (elt _pos i) s)
       )
     ;; string _rob_id
       (write-long (length _rob_id) s) (princ _rob_id s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _res
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _res (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; int32[] _pos
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _pos (instantiate integer-vector n))
     (dotimes (i n)
     (setf (elt _pos i) (sys::peek buf ptr- :integer)) (incf ptr- 4)
     ))
   ;; string _rob_id
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _rob_id (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(setf (get server::Obj :md5sum-) "bf448c37f9b2e18b1fceb325f2f29c80")
(setf (get server::Obj :datatype-) "server/Obj")
(setf (get server::Obj :definition-)
      "string res
int32[] pos
string rob_id

")



(provide :server/Obj "bf448c37f9b2e18b1fceb325f2f29c80")


