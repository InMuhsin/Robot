;; Auto-generated. Do not edit!


(when (boundp 'server::PictureRequest)
  (if (not (find-package "SERVER"))
    (make-package "SERVER"))
  (shadow 'PictureRequest (find-package "SERVER")))
(unless (find-package "SERVER::PICTUREREQUEST")
  (make-package "SERVER::PICTUREREQUEST"))

(in-package "ROS")
;;//! \htmlinclude PictureRequest.msg.html


(defclass server::PictureRequest
  :super ros::object
  :slots (_id _pos _rob_id ))

(defmethod server::PictureRequest
  (:init
   (&key
    ((:id __id) "")
    ((:pos __pos) (make-array 0 :initial-element 0 :element-type :integer))
    ((:rob_id __rob_id) "")
    )
   (send-super :init)
   (setq _id (string __id))
   (setq _pos __pos)
   (setq _rob_id (string __rob_id))
   self)
  (:id
   (&optional __id)
   (if __id (setq _id __id)) _id)
  (:pos
   (&optional __pos)
   (if __pos (setq _pos __pos)) _pos)
  (:rob_id
   (&optional __rob_id)
   (if __rob_id (setq _rob_id __rob_id)) _rob_id)
  (:serialization-length
   ()
   (+
    ;; string _id
    4 (length _id)
    ;; int32[] _pos
    (* 4    (length _pos)) 4
    ;; string _rob_id
    4 (length _rob_id)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _id
       (write-long (length _id) s) (princ _id s)
     ;; int32[] _pos
     (write-long (length _pos) s)
     (dotimes (i (length _pos))
       (write-long (elt _pos i) s)
       )
     ;; string _rob_id
       (write-long (length _rob_id) s) (princ _rob_id s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _id
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _id (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; int32[] _pos
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _pos (instantiate integer-vector n))
     (dotimes (i n)
     (setf (elt _pos i) (sys::peek buf ptr- :integer)) (incf ptr- 4)
     ))
   ;; string _rob_id
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _rob_id (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(setf (get server::PictureRequest :md5sum-) "58437850ababa37644cd94b15d68462c")
(setf (get server::PictureRequest :datatype-) "server/PictureRequest")
(setf (get server::PictureRequest :definition-)
      "string id
int32[] pos
string rob_id

")



(provide :server/PictureRequest "58437850ababa37644cd94b15d68462c")


